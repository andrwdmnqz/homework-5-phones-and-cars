package com.company.entities;

public class Person {
    private String fullName;
    private int age;
    private char sex;
    private String phoneNumber;

    public Person(String fullName, int age, char sex, String phoneNumber) {
        this.fullName = fullName;
        this.age = age;
        this.sex = sex;
        this.phoneNumber = phoneNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Person{" +
                "fullName='" + fullName + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                ", phoneNumber='" + phoneNumber + '\'' +
                "}";
    }
}
