package com.company;

import com.company.details.Engine;
import com.company.entities.Person;
import com.company.professions.Driver;
import com.company.vehicles.Lorry;
import com.company.vehicles.SportCar;

public class Main {
    public static void main(String[] args) {
        Driver driver1 = new Driver("Kotenko Bureviy Zhdanovych", 35, 'm',
                "123", 8);
        Driver driver2 = new Driver("Chuyko Gremislav Arsenovych", 28, 'm',
                "456", 5);

        Engine engine1 = new Engine(300, "HATZ");
        Engine engine2 = new Engine(500, "NIIGATA");

        Lorry lorry = new Lorry("BMW", "Truck", 40, driver1, engine1, 523);
        SportCar sportCar = new SportCar("Mercedes-AMG", "Race car", 0.746, driver2, engine2, 350);

        sportCar.start();
        lorry.start();
        lorry.turnLeft();
        sportCar.stop();

        System.out.println(lorry.toString());
        System.out.println(sportCar.toString());

    }
}
