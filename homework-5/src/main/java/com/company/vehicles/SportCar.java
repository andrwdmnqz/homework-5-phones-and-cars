package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class SportCar extends Car {
    private int speedLimit;

    public SportCar(String mark, String vehicleClass, double weight, Driver driver,
                    Engine engine, int speedLimit) {
        super(mark, vehicleClass, weight, driver, engine);
        this.speedLimit = speedLimit;
    }

    public int getSpeedLimit() {
        return speedLimit;
    }

    public void setSpeedLimit(int speedLimit) {
        this.speedLimit = speedLimit;
    }

    public void start() {
        System.out.println("Starting to accelerate to the speed limit " + speedLimit);
    }

    public void stop() {
        System.out.println("Starting to slow down from " + speedLimit + " km speed");
    }

    public void turnLeft() {
        System.out.println("Sport car turning left");
    }

    public void turnRight() {
        System.out.println("Sport car turning right");
    }

    @Override
    public String toString() {
        return super.toString() + "SportCar{" +
                "speedLimit=" + speedLimit +
                "}";
    }
}
