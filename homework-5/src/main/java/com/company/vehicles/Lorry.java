package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Lorry extends Car {
    private double bodyLoadCapacity;

    public Lorry(String mark, String vehicleClass, double weight, Driver driver,
                 Engine engine, double bodyLoadCapacity) {
        super(mark, vehicleClass, weight, driver, engine);
        this.bodyLoadCapacity = bodyLoadCapacity;
    }

    public double getBodyLoadCapacity() {
        return bodyLoadCapacity;
    }

    public void setBodyLoadCapacity(double bodyLoadCapacity) {
        this.bodyLoadCapacity = bodyLoadCapacity;
    }

    public void start() {
        System.out.println("Starting to load up to the " + bodyLoadCapacity + " kg limit");
    }

    public void stop() {
        System.out.println("Stopping loading");
    }

    public void turnLeft() {
        System.out.println("Lorry turning left");
    }

    public void turnRight() {
        System.out.println("Lorry turning right");
    }

    @Override
    public String toString() {
        return super.toString() + "Lorry{" +
                "bodyLoadCapacity=" + bodyLoadCapacity +
                "}";
    }


}
