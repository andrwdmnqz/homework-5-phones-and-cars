package com.company.professions;

import com.company.entities.Person;

public class Driver extends Person {
    private int drivingExperience;

    public Driver(String fullName, int age, char sex, String phoneNumber, int drivingExperience) {
        super(fullName, age, sex, phoneNumber);
        this.drivingExperience = drivingExperience;
    }

    public int getDrivingExperience() {
        return drivingExperience;
    }

    public void setDrivingExperience(int drivingExperience) {
        this.drivingExperience = drivingExperience;
    }

    @Override
    public String toString() {
        return super.toString() + "Driver{" +
                "drivingExperience=" + drivingExperience +
                "}";
    }
}
