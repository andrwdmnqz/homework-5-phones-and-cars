package ua.kr.shokhirev.andrii.phones;

public interface PhoneMedia {
    public String photo();
    public String video();
}
