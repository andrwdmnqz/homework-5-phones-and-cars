package ua.kr.shokhirev.andrii.phones;

public interface PhoneConnection {
    public void call(Phone phone);
    public String send(String message, Phone phone);
}
