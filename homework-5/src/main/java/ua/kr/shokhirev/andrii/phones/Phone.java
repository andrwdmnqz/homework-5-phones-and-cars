package ua.kr.shokhirev.andrii.phones;

public abstract class Phone {
    private String name;
    private String model;
    private int yearOfProduction;
    private int cameraMegaPixels;
    private double displayDiagonal;
    private String processor;
    private String phoneNumber;

    public Phone(String name, String model, int yearOfProduction, int cameraMegaPixels,
                 double displayDiagonal, String processor, String phoneNumber) {
        this.name = name;
        this.model = model;
        this.yearOfProduction = yearOfProduction;
        this.cameraMegaPixels = cameraMegaPixels;
        this.displayDiagonal = displayDiagonal;
        this.processor = processor;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", yearOfProduction=" + yearOfProduction +
                ", cameraMegaPixels=" + cameraMegaPixels +
                ", displayDiagonal=" + displayDiagonal +
                ", processor='" + processor + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYearOfProduction() {
        return yearOfProduction;
    }

    public void setYearOfProduction(int yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    public int getCameraMegaPixels() {
        return cameraMegaPixels;
    }

    public void setCameraMegaPixels(int cameraMegaPixels) {
        this.cameraMegaPixels = cameraMegaPixels;
    }

    public double getDisplayDiagonal() {
        return displayDiagonal;
    }

    public void setDisplayDiagonal(double displayDiagonal) {
        this.displayDiagonal = displayDiagonal;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public String getName() {
        return this.name;
    }

    public abstract boolean receiveCall();
}
