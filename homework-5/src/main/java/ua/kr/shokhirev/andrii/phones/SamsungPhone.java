package ua.kr.shokhirev.andrii.phones;

public class SamsungPhone extends Phone implements PhoneConnection, PhoneMedia {

    private String cameraExtension;

    public SamsungPhone(String name, String model, int yearOfProduction, int cameraMegaPixels, double displayDiagonal, String processor, String phoneNumber, String cameraExtension) {
        super(name, model, yearOfProduction, cameraMegaPixels, displayDiagonal, processor, phoneNumber);
        this.cameraExtension = cameraExtension;
    }

    @Override
    public String toString() {
        return super.toString() + "SamsungPhone{" +
                "cameraExtension='" + cameraExtension + '\'' +
                '}';
    }

    public boolean receiveCall() {
        return true;
    }

    public void call(Phone phone) {
        if (phone.getPhoneNumber() != null && phone.receiveCall()) {
            System.out.println("Calling from " + this.getName() + " to " + phone.getName());
        }
        else {
            System.out.println("Phone " + phone.getName() + " can't receive calls!");
        }
    }

    public String send(String message, Phone phone) {
        return ("Sended message \'" + message + "\' to " + phone.getName());
    }

    public String photo() {
        return "Making photo with Samsung camera!";
    }

    public String video() {
        return "Making photo with Samsung camera!";
    }

    public String getCameraExtension() {
        return cameraExtension;
    }

    public void setCameraExtension(String cameraExtension) {
        this.cameraExtension = cameraExtension;
    }
}
