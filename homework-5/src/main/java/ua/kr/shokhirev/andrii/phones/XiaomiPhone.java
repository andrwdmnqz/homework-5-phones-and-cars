package ua.kr.shokhirev.andrii.phones;

public class XiaomiPhone extends Phone implements PhoneMedia {

    public XiaomiPhone(String name, String model, int yearOfProduction, int cameraMegaPixels, double displayDiagonal, String processor, String phoneNumber) {
        super(name, model, yearOfProduction, cameraMegaPixels, displayDiagonal, processor, phoneNumber);
    }

    @Override
    public String toString() {
        return super.toString() + "XiaomiPhone{}";
    }

    public boolean receiveCall() {
        return false;
    }

    public String photo() {
        return "Making photo with Xiaomi camera!";
    }

    public String video() {
        return "Making video with Xiaomi camera!";
    }
}
