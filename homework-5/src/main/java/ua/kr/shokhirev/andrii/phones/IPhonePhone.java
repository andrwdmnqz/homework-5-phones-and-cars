package ua.kr.shokhirev.andrii.phones;

public class IPhonePhone extends Phone implements PhoneConnection {

    public IPhonePhone(String name, String model, int yearOfProduction, int cameraMegaPixels, double displayDiagonal, String processor, String phoneNumber) {
        super(name, model, yearOfProduction, cameraMegaPixels, displayDiagonal, processor, phoneNumber);
    }

    @Override
    public String toString() {
        return super.toString() + "IPhonePhone{}";
    }

    public boolean receiveCall() {
        return true;
    }

    public void call(Phone phone) {
        if (phone.getPhoneNumber() != null && phone.receiveCall()) {
            System.out.println("Calling from " + this.getName() + " to " + phone.getName());
        }
        else {
            System.out.println("Phone " + phone.getName() + " can't receive calls!");
        }
    }

    public String send(String message, Phone phone) {
        return ("Sended message \'" + message + "\' to " + phone.getName());
    }
}
