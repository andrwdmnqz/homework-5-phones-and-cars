package ua.kr.shokhirev.andrii.phones;

public class App 
{
    public static void main( String[] args )
    {
        IPhonePhone iPhone = new IPhonePhone("Iphone", "11", 2019,
                12, 6.1, "Apple A13 Bionic", "123");
        SamsungPhone samsung = new SamsungPhone("Samsung", "S21", 2022,
                108, 6.2, "Exynos 2100",
                "456", "Laser autofocus");
        XiaomiPhone xiaomi = new XiaomiPhone("Xiaomi", "12", 2021,
                50, 6.28, "Qualcomm Snapdragon 8 Gen 1", "789");

        iPhone.call(samsung);
        samsung.call(xiaomi);
        System.out.println(samsung.send("test message to xiaomi", xiaomi));
        System.out.println(xiaomi.photo());
        System.out.println(samsung.video());
        System.out.println(samsung.toString());
    }
}
